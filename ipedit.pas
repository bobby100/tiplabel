{
 *****************************************************************************
  See the file COPYING.modifiedLGPL.txt, included in this distribution,
  for details about the license.
 *****************************************************************************

 Author: Boban Spasic

}

unit IPEdit;

interface

uses
  SysUtils, Classes, Controls, StdCtrls, LResources, LCLType, StrUtils;

type
  TIPMode = (IPv4, IPv6);

  { TIPEdit }

  TIPEdit = class(TEdit)
  private

    //General
    FCaretPosition: integer;
    FOldCaretPos: integer;
    FIPMode: TIPMode;
    FReadOnly: boolean;

    //IPv4
    FCurrentOctet: integer;
    FOctetBuff: string;
    FOldOctet: integer;
    FOctets: array[1..4] of byte;

    //IPv6
    FCurrentHextet: integer;
    FHextetBuff: string;
    FOldHextet: integer;
    FHextets: array[1..8] of word;

    //General functions and procedures
    procedure Unused(const A1);
    procedure SetIPMode(m: TIPMode);
    procedure SetMaxLength(Value: integer);
    function GetMaxLength: integer;
    function GetTextPadded: string;
    function GetTextTrimmed: string;
    function SafeHex2Dec(s: string): longint;

    //IPv4 functions and procedures
    procedure SetTextFromOctets;
    procedure SetOctetsFromText;
    procedure SetOctetFocus(AOctet: integer);
    function GetCurrentOctet(ACaretPosition: integer): integer;
    function WriteOctet(AOctet: integer): boolean;
    function WriteOctetStr(AOctet: integer; Value: string): boolean;

    //IPv6 functions and procedures
    procedure SetTextFromHextets;
    procedure SetHextetsFromText;
    procedure SetHextetFocus(AHextet: integer);
    function GetCurrentHextet(ACaretPosition: integer): integer;
    function WriteHextet(AHextet: integer): boolean;
    function WriteHextetStr(AHextet: integer; Value: string): boolean;

  protected
    procedure Loaded; override;
    procedure DoEnter; override;
    procedure DoExit; override;
    procedure KeyDown(var Key: word; shift: TShiftState); override;
    procedure KeyPress(var Key: char); override;
    function GetReadOnly: boolean; override;
    procedure SetReadOnly(Value: boolean); override;
    // it is fixed to False
    procedure SetNumbersOnly(Value: boolean); override;
    // it is fixed to emNormal
    procedure SetEchoMode(Val: TEchoMode); override;

  public
    constructor Create(AOwner: TComponent); override;
    procedure Clear;
    destructor Destroy; override;
    procedure EditingDone; override;
    function IsPrivateRange: boolean;
    function IsZeroconfRange: boolean;
    property ReadOnly: boolean read GetReadOnly write SetReadOnly default False;
    property TextPadded: string read GetTextPadded;
    property TextTrimmed: string read GetTextTrimmed;

  published
    property InternetProtocol: TIPMode read FIPMode write SetIPMode default IPv4;
    // it is fixed to 15 for IPv4 or 39 for IPv6
    property MaxLength: integer read GetMaxLength write SetMaxLength default 15;
  end;

procedure Register;

implementation

// section private general

{$PUSH}{$HINTS OFF}
procedure TIPEdit.Unused(const A1);
begin
end;

{$POP}

procedure TIPEdit.SetIPMode(m: TIPMode);
begin
  FIPMode := m;

  if FIPMode = IPv4 then
    SetTextFromOctets;

  if FIPMode = IPv6 then
    SetTextFromHextets;
end;

procedure TIPEdit.SetMaxLength(Value: integer);
begin
  Unused(Value);
  if FIPMode = IPv4 then
    inherited MaxLength := 15
  else
    inherited MaxLength := 39;
end;

function TIPEdit.GetMaxLength: integer;
begin
  Result := inherited Maxlength;
end;

function TIPEdit.GetTextPadded: string;
var
  tmp: string;
begin
  Result := '';
  if FIPMode = IPv4 then
  begin
    tmp := Format('%3d.%3d.%3d.%3d', [FOctets[1], FOctets[2], FOctets[3], FOctets[4]]);
  end
  else
  begin
    tmp := Format('%4x:%4x:%4x:%4x:%4x:%4x:%4x:%4x',
      [FHextets[1], FHextets[2], FHextets[3], FHextets[4], FHextets[5],
      FHextets[6], FHextets[7], FHextets[8]]);
  end;
  Result := ReplaceStr(tmp, ' ', '0');
end;

function TIPEdit.GetTextTrimmed: string;
var
  tmp: string;
begin
  Result := '';
  if FIPMode = IPv4 then
  begin
    tmp := Format('%3d.%3d.%3d.%3d', [FOctets[1], FOctets[2], FOctets[3], FOctets[4]]);
    Result := ReplaceStr(tmp, ' ', '');
  end
  else
  begin
    tmp := Format('%4x:%4x:%4x:%4x:%4x:%4x:%4x:%4x',
      [FHextets[1], FHextets[2], FHextets[3], FHextets[4], FHextets[5],
      FHextets[6], FHextets[7], FHextets[8]]);
    tmp := tmp.Replace('   0:   0:', '::', []);
    while (pos('::   0', tmp) > 0) and (pos('::   0', tmp) = rpos('::', tmp)) do
      tmp := tmp.Replace('::   0', '::');
    while pos(':::', tmp) > 0 do
      tmp := tmp.Replace(':::', '::');
    tmp := tmp.Replace(' ', '', [rfReplaceAll]);
    Result := tmp;
  end;
end;

function TIPEdit.SafeHex2Dec(s: string): longint;
begin
  s := ReplaceStr(s, ' ', '0');
  try
    Result := Hex2Dec(s);
  except
    on e: Exception do Result := -1;
  end;
end;

// private IPv4

procedure TIPEdit.SetTextFromOctets;
begin
  Text := Format('%3d.%3d.%3d.%3d', [FOctets[1], FOctets[2], FOctets[3], FOctets[4]]);
end;

procedure TIPEdit.SetOctetsFromText;
var
  i, DotPos: integer;
  RestOfString: string;
  Value: integer;
begin
  RestOfString := Text;
  for i := Low(FOctets) to High(FOctets) do
  begin
    DotPos := Pos('.', RestOfString);
    if DotPos = 0 then
      DotPos := Length(RestOfString) + 1;
    if not TryStrToInt(Copy(RestOfString, 1, DotPos - 1), Value) or
      not (Value in [Low(byte)..High(byte)]) then
    begin
      Exit;
    end;
    FOctets[i] := Value;
    Delete(RestOfString, 1, DotPos);
  end;
end;

procedure TIPEdit.SetOctetFocus(AOctet: integer);
begin
  case AOctet of
    1: SelStart := 3;
    2: SelStart := 7;
    3: SelStart := 11;
    4: SelStart := 15;
  end;
end;

function TIPEdit.GetCurrentOctet(ACaretPosition: integer): integer;
begin
  Result := 4;
  if ACaretPosition < 13 then Result := 3;
  if ACaretPosition < 9 then Result := 2;
  if ACaretPosition < 4 then Result := 1;
end;

function TIPEdit.WriteOctet(AOctet: integer): boolean;
var
  tmp: string;
begin
  case AOctet of
    1: tmp := Copy(Text, 1, 3);
    2: tmp := Copy(Text, 5, 3);
    3: tmp := Copy(Text, 9, 3);
    4: tmp := Copy(Text, 13, 3);
  end;
  Result := WriteOctetStr(AOctet, tmp);
end;

function TIPEdit.WriteOctetStr(AOctet: integer; Value: string): boolean;
var
  tmpi: integer;
begin
  Result := False;
  tmpi := StrToIntDef(Value, 256);
  if tmpi < 256 then
  begin
    FOctets[AOctet] := tmpi;
    Result := True;
  end;
end;

// section private IPv6

procedure TIPEdit.SetTextFromHextets;
var
  tmp: string;
begin
  tmp := Format('%4x:%4x:%4x:%4x:%4x:%4x:%4x:%4x', [FHextets[1],
    FHextets[2], FHextets[3], FHextets[4], FHextets[5], FHextets[6],
    FHextets[7], FHextets[8]]);
  Text := ReplaceStr(tmp, ' ', '0');
end;

procedure TIPEdit.SetHextetsFromText;
var
  i, DotPos: integer;
  RestOfString: string;
  Value: integer;
begin
  RestOfString := Text;
  for i := Low(FHextets) to High(FHextets) do
  begin
    DotPos := Pos(':', RestOfString);
    if DotPos = 0 then
      DotPos := Length(RestOfString) + 1;
    Value := SafeHex2Dec(Copy(RestOfString, 1, DotPos - 1));
    if not (Value >= $0000) and (Value <= $ffff) then
      Exit;
    FHextets[i] := Value;
    Delete(RestOfString, 1, DotPos);
  end;
end;

procedure TIPEdit.SetHextetFocus(AHextet: integer);
begin
  case AHextet of
    1: SelStart := 4;
    2: SelStart := 9;
    3: SelStart := 14;
    4: SelStart := 19;
    5: SelStart := 24;
    6: SelStart := 29;
    7: SelStart := 34;
    8: SelStart := 39;
  end;
end;

function TIPEdit.GetCurrentHextet(ACaretPosition: integer): integer;
begin
  Result := 8;
  if ACaretPosition < 36 then Result := 7;
  if ACaretPosition < 31 then Result := 6;
  if ACaretPosition < 26 then Result := 5;
  if ACaretPosition < 21 then Result := 4;
  if ACaretPosition < 16 then Result := 3;
  if ACaretPosition < 11 then Result := 2;
  if ACaretPosition < 6 then Result := 1;
end;

function TIPEdit.WriteHextet(AHextet: integer): boolean;
var
  tmp: string;
begin
  case AHextet of
    1: tmp := Copy(Text, 1, 4);
    2: tmp := Copy(Text, 6, 4);
    3: tmp := Copy(Text, 11, 4);
    4: tmp := Copy(Text, 16, 4);
    5: tmp := Copy(Text, 21, 4);
    6: tmp := Copy(Text, 26, 4);
    7: tmp := Copy(Text, 31, 4);
    8: tmp := Copy(Text, 36, 4);
  end;
  Result := WriteHextetStr(AHextet, tmp);
end;

function TIPEdit.WriteHextetStr(AHextet: integer; Value: string): boolean;
var
  tmpi: integer;
begin
  Result := False;
  tmpi := SafeHex2Dec(trim(Value));
  if (tmpi >= 0) and (tmpi <= $ffff) then
  begin
    FHextets[AHextet] := tmpi;
    Result := True;
  end;
end;

// section protected

procedure TIPEdit.Loaded;
begin
  inherited Loaded;
  SetEchoMode(emNormal);
  SetNumbersOnly(False);
  if not Focused then
  begin
    if FIPMode = IPv4 then
    begin
      SetOctetsFromText;
      SetTextFromOctets;
      SetOctetFocus(1);
    end
    else
    begin
      SetHextetsFromText;
      SetTextFromHextets;
      SetHextetFocus(1);
    end;
  end;
end;

procedure TIPEdit.DoEnter;
begin
  inherited DoEnter;
  if FIPMode = IPv4 then
  begin
    SetOctetsFromText;
    SetTextFromOctets;
    FOctetBuff := '';
  end
  else
  begin
    SetHextetsFromText;
    SetTextFromHextets;
    FHextetBuff := '';
  end;
end;

procedure TIPEdit.DoExit;
begin
  if FIPMode = IPv4 then
  begin
    SetOctetsFromText;
    SetTextFromOctets;
    FOctetBuff := '';
  end
  else
  begin
    SetHextetsFromText;
    SetTextFromHextets;
    FHextetBuff := '';
  end;
  inherited DoExit;
end;

procedure TIPEdit.KeyDown(var Key: word; shift: TShiftState);
begin
  if FIPMode = IPv4 then
  begin
    if Key in [VK_BACK, VK_LEFT, VK_RIGHT, VK_RETURN] then
    begin
      case Key of
        VK_RIGHT: begin
          if FCurrentOctet < 4 then
          begin
            WriteOctetStr(FCurrentOctet, FOctetBuff);
            Inc(FCurrentOctet);
          end;
        end;
        VK_LEFT: begin
          if FCurrentOctet > 1 then
          begin
            WriteOctetStr(FCurrentOctet, FOctetBuff);
            Dec(FCurrentOctet);
          end;
        end;
        VK_BACK: begin
          WriteOctetStr(FCurrentOctet, '0');
          SetOctetFocus(FCurrentOctet);
        end;
        VK_RETURN: begin
          SetOctetsFromText;
          SetOctetFocus(1);
        end;
      end;
      FOctetBuff := '';
      SetTextFromOctets;
      SetOctetFocus(FCurrentOctet);
      Key := 0;
    end;
  end
  else
  begin
    if Key in [VK_BACK, VK_LEFT, VK_RIGHT, VK_RETURN] then
    begin
      case Key of
        VK_RIGHT: begin
          if FCurrentHextet < 8 then
          begin
            WriteHextetStr(FCurrentHextet, FHextetBuff);
            Inc(FCurrentHextet);
          end;
        end;
        VK_LEFT: begin
          if FCurrentHextet > 1 then
          begin
            WriteHextetStr(FCurrentHextet, FHextetBuff);
            Dec(FCurrentHextet);
          end;
        end;
        VK_BACK: begin
          WriteHextetStr(FCurrentHextet, '0');
          SetHextetFocus(FCurrentHextet);
        end;
        VK_RETURN: begin
          SetHextetsFromText;
          SetHextetFocus(1);
        end;
      end;
      FHextetBuff := '';
      SetTextFromHextets;
      SetHextetFocus(FCurrentHextet);
      Key := 0;
    end;
  end;
  inherited KeyDown(Key, Shift);
end;

procedure TIPEdit.KeyPress(var Key: char);
var
  TestBuffer: string;
begin
  if not FReadOnly then
  begin
    if FIPMode = IPv4 then
    begin
      if Key = '.' then
      begin
        WriteOctetStr(FCurrentOctet, FOctetBuff);
        FOctetBuff := '';
        Inc(FCurrentOctet);
        SetTextFromOctets;
        SetOctetFocus(FCurrentOctet);
      end;
      if Key in ['0'..'9'] then
      begin
        TestBuffer := FOctetBuff + Key;
        if StrToIntDef(TestBuffer, 256) < 256 then
        begin
          FOctetBuff := FOctetBuff + Key;
          FOldCaretPos := SelStart;
          FOldOctet := GetCurrentOctet(FOldCaretPos);
          if WriteOctetStr(FOldOctet, FOctetBuff) then
          begin
            SetTextFromOctets;
            FCurrentOctet := FOldOctet;
            SetOctetFocus(FCurrentOctet);
          end;
          if Length(FOctetBuff) < 3 then
            SelStart := FOldCaretPos
          else
          begin
            FOctetBuff := '';
            Inc(FCurrentOctet);
            SetOctetFocus(FCurrentOctet);
          end;
          SetTextFromOctets;
          Key := #0;
        end;
      end
      else
        Key := #0;
    end
    else
    begin
      if Key = ':' then
      begin
        WriteHextetStr(FCurrentHextet, FHextetBuff);
        FHextetBuff := '';
        Inc(FCurrentHextet);
        SetTextFromHextets;
        SetHextetFocus(FCurrentHextet);
      end;
      if (Key in ['0'..'9']) or (Key in ['a'..'f']) or (Key in ['A'..'F']) then
      begin
        TestBuffer := FHextetBuff + Key;
        if (SafeHex2Dec(TestBuffer) >= $0000) and (SafeHex2Dec(TestBuffer) <= $ffff) then
        begin
          FHextetBuff := FHextetBuff + Key;
          FOldCaretPos := SelStart;
          FOldHextet := GetCurrentHextet(FOldCaretPos);
          if WriteHextetStr(FOldHextet, FHextetBuff) then
          begin
            SetTextFromHextets;
            FCurrentHextet := FOldHextet;
            SetHextetFocus(FCurrentHextet);
          end;
          if Length(FHextetBuff) < 4 then
            SelStart := FOldCaretPos
          else
          begin
            FHextetBuff := '';
            Inc(FCurrentHextet);
            SetHextetFocus(FCurrentHextet);
          end;
          SetTextFromHextets;
          Key := #0;
        end;
      end
      else
        Key := #0;
    end;
    inherited KeyPress(Key);
  end;
end;

function TIPEdit.GetReadOnly: boolean;
begin
  Result := FReadOnly;
end;

procedure TIPEdit.SetReadOnly(Value: boolean);
begin
  FReadOnly := Value;
  inherited SetReadOnly(Value);
end;

procedure TIPEdit.SetNumbersOnly(Value: boolean);
begin
  Unused(Value);
  inherited SetNumbersOnly(False);
end;

procedure TIPEdit.SetEchoMode(Val: TEchoMode);
begin
  Unused(Val);
  inherited SetEchoMode(emNormal);
end;

// section published

constructor TIPEdit.Create(AOwner: TComponent);
var
  i: integer;
begin
  inherited Create(AOwner);

  if (csDesigning in ComponentState) then
  begin
    Modified := False;
    MaxLength := 15;
    NumbersOnly := False;
    EchoMode := emNormal;
  end;

  FCaretPosition := 0;
  FOldCaretPos := 0;
  FCurrentOctet := 1;
  FOldOctet := 1;
  FOctetBuff := '';
  FCurrentHextet := 1;
  FOldHextet := 1;
  FHextetBuff := '';

  for i := 1 to 4 do
    FOctets[i] := 0;
  if FIPMode = IPv4 then
  begin
    SetOctetsFromText; //take the vales from Object Inspector if any
    SetTextFromOctets;
  end;

  for i := 1 to 8 do
    FHextets[i] := 0;
  if FIPMode = IPv6 then
  begin
    SetHextetsFromText;
    SetTextFromHextets;
  end;
end;

procedure TIPEdit.Clear;
var
  i: integer;
begin
  FCaretPosition := 0;
  FOldCaretPos := 0;
  FCurrentOctet := 1;
  FOldOctet := 1;
  FOctetBuff := '';
  FCurrentHextet := 1;
  FOldHextet := 1;
  FHextetBuff := '';

  for i := 1 to 4 do
    FOctets[i] := 0;
  if FIPMode = IPv4 then
    SetTextFromOctets;

  for i := 1 to 8 do
    FHextets[i] := 0;
  if FIPMode = IPv6 then
    SetTextFromHextets;
end;

destructor TIPEdit.Destroy;
begin
  inherited Destroy;
end;

procedure TIPEdit.EditingDone;
begin
  if FIPMode = IPv4 then
  begin
    SetOctetsFromText;
    SetTextFromOctets;
    FOctetBuff := '';
  end
  else
  begin
    SetHextetsFromText;
    SetTextFromHextets;
    FHextetBuff := '';
  end;
  inherited EditingDone;
end;

function TIPEdit.IsPrivateRange: boolean;
const
  p11: string = ' 10.  0.  0.  0';
  p12: string = ' 10.255.255.255';
  p21: string = '172. 16.  0.  0';
  p22: string = '172. 31.255.255';
  p31: string = '192.168.  0.  0';
  p32: string = '192.168.255.255';
  p61: string = 'FC00:0000:0000:0000:0000:0000:0000:0000';
  p62: string = 'FDFF:FFFF:FFFF:FFFF:FFFF:FFFF:FFFF:FFFF';
var
  p: string;
begin
  Result := False;
  if FIPMode = IPv4 then
  begin
    p := Format('%3d.%3d.%3d.%3d', [FOctets[1], FOctets[2], FOctets[3], FOctets[4]]);
    if ((CompareStr(p, p11) > -1) and (CompareStr(p, p12) < 1)) or
      ((CompareStr(p, p21) > -1) and (CompareStr(p, p22) < 1)) or
      ((CompareStr(p, p31) > -1) and (CompareStr(p, p32) < 1)) then Result := True;
  end
  else
  begin
    p := Format('%4x:%4x:%4x:%4x:%4x:%4x:%4x:%4x',
      [FHextets[1], FHextets[2], FHextets[3], FHextets[4], FHextets[5],
      FHextets[6], FHextets[7], FHextets[8]]);
    p := ReplaceStr(p, ' ', '0');
    if (CompareStr(p, p61) > -1) and (CompareStr(p, p62) < 1) then Result := True;
  end;
end;

function TIPEdit.IsZeroconfRange: boolean;
const
  p11: string = '169.254.  0.  0';
  p12: string = '169.254.255.255';
  p61: string = 'FE80:0000:0000:0000:0000:0000:0000:0000';
  p62: string = 'FEBF:FFFF:FFFF:FFFF:FFFF:FFFF:FFFF:FFFF';
var
  p: string;
begin
  Result := False;
  if FIPMode = IPv4 then
  begin
    p := Format('%3d.%3d.%3d.%3d', [FOctets[1], FOctets[2], FOctets[3], FOctets[4]]);
    if (CompareStr(p, p11) > -1) and (CompareStr(p, p12) < 1) then Result := True;
  end
  else
  begin
    p := Format('%4x:%4x:%4x:%4x:%4x:%4x:%4x:%4x',
      [FHextets[1], FHextets[2], FHextets[3], FHextets[4], FHextets[5],
      FHextets[6], FHextets[7], FHextets[8]]);
    p := ReplaceStr(p, ' ', '0');
    if (CompareStr(p, p61) > -1) and (CompareStr(p, p62) < 1) then Result := True;
  end;
end;

procedure Register;
begin
  RegisterComponents('Misc', [TIPEdit]);
  {$I ipedit.lrs}
end;

end.
