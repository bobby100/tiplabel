unit Unit1;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Forms, Controls, Graphics, Dialogs, StdCtrls, IPEdit,
  LCLType;

type

  { TForm1 }

  TForm1 = class(TForm)
    Button1: TButton;
    CheckBox1: TCheckBox;
    CheckBox2: TCheckBox;
    CheckBox3: TCheckBox;
    CheckBox4: TCheckBox;
    IPEdit1: TIPEdit;
    Memo1: TMemo;
    procedure Button1Click(Sender: TObject);
    procedure CheckBox3Change(Sender: TObject);
    procedure CheckBox4Change(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure IPEdit1Change(Sender: TObject);
    procedure IPEdit1EditingDone(Sender: TObject);
  private

  public

  end;

var
  Form1: TForm1;

implementation

{$R *.lfm}

{ TForm1 }

procedure TForm1.IPEdit1EditingDone(Sender: TObject);
begin
  memo1.lines.add('Caption: ' + IPEdit1.Text);
  memo1.lines.add('Padded: ' + IPEdit1.TextPadded);
  memo1.lines.add('Trimmed: ' + IPEdit1.TextTrimmed);
end;

procedure TForm1.IPEdit1Change(Sender: TObject);
begin
  CheckBox1.Checked:=IPEdit1.IsPrivateRange;
  CheckBox2.Checked:=IPEdit1.IsZeroconfRange;
end;

procedure TForm1.CheckBox3Change(Sender: TObject);
begin
  if CheckBox3.Checked then IPEdit1.InternetProtocol:=IPv6 else IPEdit1.InternetProtocol:=IPv4;
end;

procedure TForm1.CheckBox4Change(Sender: TObject);
begin
  IPEdit1.ReadOnly:=CheckBox4.Checked;
end;

procedure TForm1.Button1Click(Sender: TObject);
begin
  IPEdit1.Clear;
end;

procedure TForm1.FormShow(Sender: TObject);
begin
  if IPEdit1.InternetProtocol = IPv6 then CheckBox3.Checked := true else CheckBox3.Checked := false;
end;


end.
