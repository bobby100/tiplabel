# TIPEdit

Lazarus/Freepascal component for entering IPv4 and IPv6 addresses.

Descendant of TEdit.. The component installs to Misc. tab.

It has two modes (property InternetProtocol): IPv4 and IPv6

Every octet of the IPv4 address can have a value between 0 and 255.
Every hextet of the IPv6 address can have a value between $0000 and $ffff.

Cursor keys Left and Right and also the Period/Colon key are used to navigate between the octets/hextets.
Apart from TIPEdit.Text, there are also TIPEdit.TextPadded and TIPEdit.TextTrimmed outputs:

```
Text:         192.168.  1.  1
TextPadded:   192.168.001.001
TextTrimmed:  192.168.1.1

Text:         FDFC:0000:0000:0000:0055:0000:0000:0001
TextPadded:   FDFC:0000:0000:0000:0055:0000:0000:0001
TextTrimmed:  FDFC::55:0:0:1
```

Additional functions are available:
- IsPrivateRange (IPv4 and IPv6)
- IsZeroconfRange (IPv4 and IPv6)

Because it is a descendant from TEdit, some published properties of TEdit are fixed not to interfere:
- NumbersOnly is false
- MaxLength is 15 (IPv4) or 39 (IPv6)
- EchoMode is emNormal
